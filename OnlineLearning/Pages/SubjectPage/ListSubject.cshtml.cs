using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.SubjectPage
{
    public class ListSubjectModel : PageModel
    {
        private readonly SWPContext _context;

        public List<Subject> listSubject { get; set; }

        public ListSubjectModel(SWPContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            listSubject = await _context.Subjects.ToListAsync();
            return Page();
        }
    }
}
