using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OnlineLearning.Models;

namespace OnlineLearning.Pages.Auth
{
    public class RegisterModel : PageModel
    {
        private readonly SWPContext _context;

        public RegisterModel(SWPContext context)
        {
            _context = context;
        }
        private static Random random = new Random();


        [BindProperty]
        public Models.Profile Profile { get; set; } = default!;


        [BindProperty]
        public Models.Account Account { get; set; } = default!;

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                var _user = _context.Accounts.Where(x => x.Email == Account.Email).FirstOrDefault();
                if (_user == null)
                {
                    const string chars = "0123456789";
                    var id = new string(Enumerable.Repeat(chars, 5)
                        .Select(s => s[random.Next(s.Length)]).ToArray());

                    Profile.Name = Profile.Name ?? "";
                    Profile.ProfileId = Int32.Parse(id);
                    Profile.RoleId = 1;
                    _context.Profiles.Add(Profile);
                    _context.SaveChanges();
                 
                    Account.AccountId = Int32.Parse(id);
                    Account.RoleId = 1;
                    _context.Accounts.Add(Account);
                    _context.SaveChanges();

                    return RedirectToPage("./SignIn");
                }
                else
                {
                    ViewData["error"] = "User is already exist !!!!";
                    return Page();
                }
            }
            else
            {
                ModelState.Clear();
            }
            return Page();

        }
    }
}
