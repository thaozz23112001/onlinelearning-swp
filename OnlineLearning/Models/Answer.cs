﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Answer
    {
        public Answer()
        {
            FlashCards = new HashSet<FlashCard>();
        }

        public int AnswerId { get; set; }
        public string Answer1 { get; set; } = null!;
        public int FlashcardId { get; set; }

        public virtual FlashCard Flashcard { get; set; } = null!;
        public virtual ICollection<FlashCard> FlashCards { get; set; }
    }
}
