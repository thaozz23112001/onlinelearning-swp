﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Role
    {
        public Role()
        {
            Accounts = new HashSet<Account>();
            Profiles = new HashSet<Profile>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; } = null!;

        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
