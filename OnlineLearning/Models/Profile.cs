﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Profile
    {
        public Profile()
        {
            Comments = new HashSet<Comment>();
            ProfileCourses = new HashSet<ProfileCourse>();
            TestProfiles = new HashSet<TestProfile>();
        }

        public int ProfileId { get; set; }
        public string Name { get; set; } = null!;
        public string? Phone { get; set; }
        public DateTime? Birthday { get; set; }
        public string? Address { get; set; }
        public bool? Gender { get; set; }
        public bool? Blocked { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; } = null!;
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<ProfileCourse> ProfileCourses { get; set; }
        public virtual ICollection<TestProfile> TestProfiles { get; set; }
    }
}
