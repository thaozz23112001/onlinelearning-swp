﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class TestProfile
    {
        public string Code { get; set; } = null!;
        public int ProfileId { get; set; }
        public int Mark { get; set; }
        public DateTime SuccessTime { get; set; }

        public virtual Test CodeNavigation { get; set; } = null!;
        public virtual Profile Profile { get; set; } = null!;
    }
}
