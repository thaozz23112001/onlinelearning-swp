﻿using System;
using System.Collections.Generic;

namespace OnlineLearning.Models
{
    public partial class Comment
    {
        public int CommentId { get; set; }
        public int ProfileId { get; set; }
        public string Content { get; set; } = null!;
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? CourseId { get; set; }
        public bool? Status { get; set; }

        public virtual Profile Profile { get; set; } = null!;
    }
}
